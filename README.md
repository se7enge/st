# A Custom 'st' Build

![terminal preview](./previews/st_preview.png)

## About
[st](https://st.suckless.org/) is a fast, minimal, no-nonsense terminal emulator, written entirely in C, for the X Window System for Unix-like operating systems; developed by [suckless](https://suckless.org/philosophy/).

Many consider st to be "too minimal" out the box for standard applications and therefore largely unusable. This is my personal build/fork of st which seeks to address that "issue" by adding in extra features that most users expect and prefer. This build, therefore, has many community patches pre-applied for saner defaults and greater usability.

### Patches included in this build:
+ alpha (for optional transparency)
+ anysize (to allow for better dynamic resizing; which also fixes spacing issues on tiling window managers)
+ font2 (adds the option for a secondary font to utilise alongside the primary)
+ hidecursor (hides the cursor upon detection of keystroke)
+ scrollback (adds basic scrollback functionality)
+ scrollback-reflow (allow column and row reflow w/ scrollback)
+ scrollback-mouse (allow scrollback w/ `Shift+Mousewheel`)
+ scrollback-mouse-altscreen (allow scrollback w/ `Mousewheel` only)
+ scrollback-mouse-increment (allow changing the speed of mouse scrolling)
+ ligatures-alpha-scrollback (adds better drawing of ligatures using harfbuzz library)
+ vertcenter (adds better vertical alignment of lines)
+ workingdir (adds `-d` flag to allow launching st from specified directory)

## Dependencies
+ nerd-font-noto-sans-mono
+ ttf-hack-nerd
+ harfbuzz

## Installation
```
$ git clone https://gitlab.com/se7enge/st.git
$ cd st
$ sudo make clean install
```

## Keybindings
See `config.h` or `config.def.h`.
